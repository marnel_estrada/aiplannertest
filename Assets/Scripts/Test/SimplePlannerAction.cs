﻿using Unity.AI.Planner;
using Unity.AI.Planner.Agent;
using Unity.Entities;

using UnityEngine;

namespace Game {
    public abstract class SimplePlannerAction : IOperationalAction<SimplePlanner> {
        public abstract void BeginExecution(Entity stateEntity, ActionContext action, SimplePlanner agent);
        public abstract void ContinueExecution(Entity stateEntity, ActionContext action, SimplePlanner agent);
        public abstract void EndExecution(Entity stateEntity, ActionContext action, SimplePlanner agent);
        public abstract OperationalActionStatus Status(Entity stateEntity, ActionContext action, SimplePlanner agent);
    }
}
