using Unity.AI.Planner;
using Unity.AI.Planner.Agent;
using Unity.Entities;

using UnityEngine;

namespace Game {
    public class TestAction : SimplePlannerAction {
        public override void BeginExecution(Entity stateEntity, ActionContext action, SimplePlanner agent) {
            Debug.Log("TestAction.Begin");
        }

        public override void ContinueExecution(Entity stateEntity, ActionContext action, SimplePlanner agent) {
            Debug.Log("TestAction.Continue");
        }

        public override void EndExecution(Entity stateEntity, ActionContext action, SimplePlanner agent) {
            Debug.Log("TestAction.End");
        }

        public override OperationalActionStatus Status(Entity stateEntity, ActionContext action, SimplePlanner agent) {
            return OperationalActionStatus.Completed;
        }
    }
}
