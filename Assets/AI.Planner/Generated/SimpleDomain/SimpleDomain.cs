using System;
using System.Collections.Generic;
using Unity.AI.Planner;
using Unity.AI.Planner.DomainLanguage.TraitBased;
using Unity.Entities;

namespace SimpleDomain
{
    [Flags]
    public enum TraitMask : uint  // Can change enum backing depending on number of traits.
    {
        None = 0,
        Condition = 1,
    }

    public static class TraitMaskUtility
    {
        public static uint GetTraitMask(params Type[] traitFilter)
        {
            var traitMask = TraitMask.None;
            foreach (var trait in traitFilter)
            {
                switch (trait.Name)
                {
                    case nameof(Condition):
                        traitMask |= TraitMask.Condition;
                        break;

                }
            }

            return (uint)traitMask;
        }
    }

    public class SimpleDomainUpdateSystem : PolicyGraphUpdateSystem
    {
        List<Entity> m_EntityListLHS = new List<Entity>();
        List<Entity> m_EntityListRHS = new List<Entity>();

        ComponentType ConditionTrait;

        bool zeroSizedCondition;

        protected override void OnCreateManager()
        {
            base.OnCreateManager();

            ConditionTrait = ComponentType.Create<Condition>();

            zeroSizedCondition = ConditionTrait.IsZeroSized;
        }

        internal override HashCode HashState(Entity stateEntity)
        {
            var domainObjectBuffer = EntityManager.GetBuffer<DomainObjectReference>(stateEntity);
            var domainObjectHashCodes = GetComponentDataFromEntity<HashCode>();
            
            // h = 3860031 + (h+y)*2779 + (h*y*2)   // from How to Hash a Set by Richard O’Keefe
            var stateHashValue = 0;

            for (var i = 0; i < domainObjectBuffer.Length; i++)
            {
                var objHash = domainObjectHashCodes[domainObjectBuffer[i].DomainObjectEntity].Value;
                stateHashValue = 3860031 + (stateHashValue + objHash) * 2779 + (stateHashValue * objHash * 2);
            }

            var stateHashCode = domainObjectHashCodes[stateEntity];
            var traitMask = (TraitMask) stateHashCode.TraitMask;

            if ((traitMask & TraitMask.Condition) != 0)
            {
                var traitHash = EntityManager.GetComponentData<Condition>(stateEntity).GetHashCode();
                stateHashValue = 3860031 + (stateHashValue + traitHash) * 2779 + (stateHashValue * traitHash * 2);
            }

            stateHashCode.Value = stateHashValue;
            return stateHashCode;
        }

        protected override bool StateEquals(Entity lhsStateEntity, Entity rhsStateEntity)
        {
            m_EntityListLHS.Clear();
            m_EntityListRHS.Clear();
            
            // Easy check is to make sure each state has the same number of domain objects
            var lhsObjectBuffer = EntityManager.GetBuffer<DomainObjectReference>(lhsStateEntity);
            var rhsObjectBuffer = EntityManager.GetBuffer<DomainObjectReference>(rhsStateEntity);
            if (lhsObjectBuffer.Length != rhsObjectBuffer.Length)
                return false;
            
            for (var i = 0; i < lhsObjectBuffer.Length; i++)
            {
                m_EntityListLHS.Add(lhsObjectBuffer[i].DomainObjectEntity);
                m_EntityListRHS.Add(rhsObjectBuffer[i].DomainObjectEntity);
            }
            
            // Next, check that each object has at least one match (by hash/checksum/trait mask)
            var entityHashCodes = GetComponentDataFromEntity<HashCode>();
            var lhsHashCode = entityHashCodes[lhsStateEntity];
            var rhsHashCode = entityHashCodes[rhsStateEntity];
            if (lhsHashCode != rhsHashCode)
                return false;
            
            for (var index = 0; index < m_EntityListLHS.Count; index++)
            {
                var entityLHS = m_EntityListLHS[index];
            
                // Check for any objects with matching hash code.
                var hashLHS = entityHashCodes[entityLHS];
                var foundMatch = false;
                for (var rhsIndex = 0; rhsIndex < m_EntityListRHS.Count; rhsIndex++)
                {
                    var entityRHS = m_EntityListRHS[rhsIndex];
                    if (hashLHS == entityHashCodes[entityRHS])
                    {
                        foundMatch = true;
                        break;
                    }
                }
            
                // No matching object found.
                if (!foundMatch)
                    return false;
            }
            
            // todo do not need to grab zero-sized components
            var Conditions = GetComponentDataFromEntity<Condition>(true);

            
            while (m_EntityListLHS.Count > 0)
            {
                var entityLHS = m_EntityListLHS[0];
            
                // Check for any objects with matching hash code.
                var hashLHS = entityHashCodes[entityLHS];
                var firstMatchIndex = -1;
                for (var rhsIndex = 0; rhsIndex < m_EntityListRHS.Count; rhsIndex++)
                {
                    var entityRHS = m_EntityListRHS[rhsIndex];
                    if (hashLHS == entityHashCodes[entityRHS])
                    {
                        firstMatchIndex = rhsIndex;
                        break;
                    }
                }
            
                var traitMask = (TraitMask)hashLHS.TraitMask;
            
                var hasCondition = (traitMask & TraitMask.Condition) != 0;

            
                var foundMatch = false;
                for (var rhsIndex = firstMatchIndex; rhsIndex < m_EntityListRHS.Count; rhsIndex++)
                {
                    var entityRHS = m_EntityListRHS[rhsIndex];
                    if (hashLHS != entityHashCodes[entityRHS])
                        continue;
            
                    if (hasCondition && !zeroSizedCondition && !Conditions[entityLHS].Equals(Conditions[entityRHS]))
                        continue;

            
                    m_EntityListLHS.RemoveAt(0);
                    m_EntityListRHS.RemoveAt(rhsIndex);
                    foundMatch = true;
                    break;
                }
            
                if (!foundMatch)
                    return false;
            }
            return true;
        }
    }
}
