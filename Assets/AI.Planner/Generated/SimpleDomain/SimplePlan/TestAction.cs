using System.Collections.Generic;
using Unity.AI.Planner;
using Unity.AI.Planner.DomainLanguage.TraitBased;
using Unity.Collections;
using Unity.Entities;

namespace SimpleDomain.SimplePlan
{
    public partial class TestAction : BaseAction<TestAction.Permutation>
    {
        public NativeArray<ComponentType> AgentTypes { get; private set; }

        List<(Entity, int)> m_AgentEntities = new List<(Entity, int)>();

        public struct Permutation
        {
            public int AgentIndex;
            public int Length => 1;

            public int this[int i]
            {
                get
                {
                    switch(i)
                    {
                        case 0:
                            return AgentIndex;

                    }

                    return -1;
                }
            }
        }

        protected override void OnCreateManager()
        {
            base.OnCreateManager();

            AgentTypes = new NativeArray<ComponentType>(new []
            {
                ComponentType.ReadOnly<Condition>(),
            }, Allocator.Persistent);


            m_FilterTuples = new[]
            {
                (AgentTypes, m_AgentEntities),
            };
        }

        protected override void OnDestroyManager()
        {
            base.OnDestroyManager();

            if (AgentTypes.IsCreated)
            {
                AgentTypes.Dispose();
                AgentTypes = default;
            }
        }

        protected override void GenerateArgumentPermutations(Entity stateEntity)
        {
            var DomainObjects = GetComponentDataFromEntity<DomainObjectTrait>(true);
            var Conditions = GetComponentDataFromEntity<Condition>(true);

            FilterObjects(stateEntity);
            foreach (var (AgentEntity, AgentIndex) in m_AgentEntities)
            {
                if (!(Conditions[AgentEntity].Value == 0))
                    continue;
                m_ArgumentPermutations.Add(new Permutation()
                {
                    AgentIndex = AgentIndex,
                });
            }
        }

        protected override void ApplyEffects(Permutation permutation, Entity parentPolicyGraphNodeEntity, Entity originalStateEntity, int horizon)
        {
            var actionNodeEntity = CreateActionNode(parentPolicyGraphNodeEntity);
            var stateCopyEntity = CopyState(originalStateEntity);

            var domainObjectBuffer = EntityManager.GetBuffer<DomainObjectReference>(stateCopyEntity);
            var DomainObjects = GetComponentDataFromEntity<DomainObjectTrait>();
            var Conditions = GetComponentDataFromEntity<Condition>();
            var objectHashes = GetComponentDataFromEntity<HashCode>();

            var AgentEntity = GetEntity(domainObjectBuffer, permutation.AgentIndex, stateCopyEntity);
            
            {
                var @Agent = Conditions[AgentEntity];
                var hash = objectHashes[AgentEntity];
                hash.Value -= @Agent.GetHashCode();
                @Agent.Value = 1;
                Conditions[AgentEntity] = @Agent;
                hash.Value += @Agent.GetHashCode();
                objectHashes[AgentEntity] = hash;
            }

            var argumentLength = permutation.Length;
            var argumentBuffer = EntityManager.GetBuffer<ActionNodeArgument>(actionNodeEntity);
            var arguments = new NativeArray<int>(argumentLength, Allocator.Temp);
            for (var i = 0; i < argumentLength; i++)
            {
                var argumentIndex = permutation[i];
                argumentBuffer.Add(new ActionNodeArgument() { DomainObjectReferenceIndex = argumentIndex });
                arguments[i] = argumentIndex;
            }

            ApplyCustomActionEffectsToState(stateCopyEntity, originalStateEntity, arguments);

            SetActionData(stateCopyEntity, originalStateEntity, parentPolicyGraphNodeEntity, horizon + 1, actionNodeEntity,
                Reward(originalStateEntity, stateCopyEntity, arguments));

            arguments.Dispose();
        }

        partial void ApplyCustomActionEffectsToState(Entity stateEntity, Entity originalStateEntity, NativeArray<int> arguments); // Implement this method in another file to extend the action's effects

        float Reward(Entity startStateEntity, Entity endStateEntity, NativeArray<int> arguments)
        {
            var reward = 0f;
            SetCustomReward(ref reward, startStateEntity, endStateEntity, arguments);

            return reward;
        }

        partial void SetCustomReward(ref float reward, Entity startStateEntity, Entity endStateEntity, NativeArray<int> arguments); // Implement this method in another file to modify the reward
    }
}
