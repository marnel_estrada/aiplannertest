using System;
using Unity.AI.Planner;
using Unity.AI.Planner.DomainLanguage.TraitBased;
using Unity.Entities;

namespace SimpleDomain
{
    [Serializable]
    public partial struct Condition : ITrait<Condition>, IEquatable<Condition>
    {
        public System.Int64 m_Value;

        public bool Equals(Condition other)
        {
            return m_Value == other.m_Value;
        }

        public override int GetHashCode()
        {
            return 397
                ^ m_Value.GetHashCode();
        }

        public object Clone() { return MemberwiseClone(); }
        public void SetComponentData(EntityManager entityManager, Entity domainObjectEntity)
        {
            SetTraitMask(entityManager, domainObjectEntity);
            entityManager.SetComponentData(domainObjectEntity, this);
        }

        public void SetTraitMask(EntityManager entityManager, Entity domainObjectEntity)
        {
            var objectHash = entityManager.GetComponentData<HashCode>(domainObjectEntity);
            objectHash.TraitMask = objectHash.TraitMask | (uint)TraitMask.Condition;
            entityManager.SetComponentData(domainObjectEntity, objectHash);
        }
    }
}
